package bot;

import util.Tools;

public enum Message {
  HELP("Per ricevere il QR code invia il testo o il link desiderato\n" +
      "/wifi - per creare il QR code di una connesisone WiFi\n" +
      "/help - mostra il messaggio di help"),
  START("Grazie per aver scelto il mio bot.\n" +
      "Questo bot è in grado di inviare il QR code del testo o link desiderato, basterà inviarlo come messaggio e in risposta invierà il QR code."),
  ASK_WIFI("Confermi di voler creare un QR code di un WiFi?\n" +
      "/si\n/no"),
  NOTHING("Ok, non farò nulla."),
  IN_PROGRESS("Questa funzione è in fase di sviluppo."),
  ASK_WIFI_TYPE("Che tipo di sicurezza ha il WiFi?\n" +
      "/wpa - per WiFi di tipo WPA o WPA2 (più comune)\n" +
      "/wep - per WiFi di tipo WEP\n" +
      "/free - se si tratta di un WiFi senza password"),
  WPA_SET("WPA impostato, ora inviami il nome del WiFi"),
  WEP_SET("WEP impostato, ora inviami il nome del WiFi"),
  FREE_SET("WiFi libero impostato, ora inviami il nome del WiFi"),
  WIFI_NAME_SET("Nome del WiFi impostato, ora inviami la password."),
  HOME("Ok, sei alla home");

  private String msg;

  private Message(String str) {
    msg = str;
  }

  public String toString() {
    return Tools.convertUTF8(msg);
  }
}
