package bot;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import util.QRCodeGen;
import util.db.DBHelper;
import util.db.User;
import util.db.WiFiQR;

public class QrCodeBot extends TelegramLongPollingBot {
  QRCodeGen qrCodeGen = new QRCodeGen("Q", "#ffffff", "10", "px", "300", "medium");
  DBHelper dbh = new DBHelper();

  @Override
  public void onUpdateReceived(Update update) {
    if (update.hasMessage() && update.getMessage().hasText()) {
      long chatId = update.getMessage().getChatId();
      String text = update.getMessage().getText();
      SendMessage msg = new SendMessage().setChatId(chatId);
      if (!dbh.checkUser(chatId)) {
        dbh.save(new User(chatId, "home"));
      }
      if (text.equals("/home")) {
        dbh.delete(chatId);
        dbh.setState(chatId, "home");
        msg.setText(String.valueOf(Message.HOME));
      }
      if (text.equals("/help"))
        msg.setText(String.valueOf(Message.HELP));
      switch (dbh.getState(chatId)) {
        case "home":
          switch (text) {
            case "/start":
              msg.setText(String.valueOf(Message.START));
              break;
            case "/help":
              msg.setText(String.valueOf(Message.HELP));
              break;
            case "/wifi":
              msg.setText(String.valueOf(Message.ASK_WIFI));
              dbh.setState(chatId, "wifi_qr");
              break;
            case "/home":
              msg.setText(String.valueOf(Message.HOME));
              break;
            default:
              qrCodeGen.setData(text);
              msg.setText(qrCodeGen.getCode());
          }
          break;
        case "wifi_qr":
          WiFiQR wfqr;
          if (text.equals("/si")) {
            msg.setText(String.valueOf(Message.ASK_WIFI_TYPE));
            dbh.save(new WiFiQR(chatId));
            dbh.setState(chatId, "wifi_type");
          } else {
            msg.setText(String.valueOf(Message.NOTHING));
            dbh.setState(chatId, "home");
          }
          break;
        case "wifi_type":
          switch (text) {
            case "/wpa":
              msg.setText(String.valueOf(Message.WPA_SET));
              dbh.setType(chatId, "WPA");
              dbh.setState(chatId, "wifi_name_ask");
              break;
            case "/wep":
              msg.setText(String.valueOf(Message.WEP_SET));
              dbh.setType(chatId, "WEP");
              dbh.setState(chatId, "wifi_name_ask");
              break;
            case "/free":
              msg.setText(String.valueOf(Message.FREE_SET));
              dbh.setState(chatId, "wifi_name_ask");
              break;
            default:
              msg.setText(String.valueOf(Message.ASK_WIFI_TYPE));
          }
          break;
        case "wifi_name_ask":
          if (dbh.getType(chatId) != null) {
            msg.setText(String.valueOf(Message.WIFI_NAME_SET));
            dbh.setSSID(chatId, text);
            dbh.setState(chatId, "wifi_pass_ask");
          } else {
            dbh.setSSID(chatId, text);
            wfqr = dbh.getWiFi(chatId);
            qrCodeGen.setData(wfqr.toString());
            msg.setText(qrCodeGen.getCode());
            dbh.delete(chatId);
            dbh.setState(chatId, "home");
          }
          break;
        case "wifi_pass_ask":
          wfqr = dbh.getWiFi(chatId);
          wfqr.setPassword(text);
          qrCodeGen.setData(wfqr.toString());
          msg.setText(qrCodeGen.getCode());
          dbh.delete(chatId);
          dbh.setState(chatId, "home");
          break;
      }

//      System.out.println(dbh.getState(chatId));// TODO - debug
      try {
        execute(msg);
      } catch (TelegramApiException e) {
        e.printStackTrace();
      }
    }
  }

  @Override
  public String getBotUsername() {
    return "QRCodeGenBot";
  }

  @Override
  public String getBotToken() {
    return System.getenv("TOKEN");
  }
}
