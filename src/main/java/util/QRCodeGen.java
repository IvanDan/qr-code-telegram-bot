package util;

public class QRCodeGen {
  private StringBuilder url;
  private String data;

  /**
   * @param errorCorrection il livello di correzione del codice, dal più basso al più alto: <code>L</code>, <code>M</code>, <code>Q</code>, <code>H</code>
   * @param backColor       il colore del background in esadecimale
   * @param quietZone       il valore di quiet zone
   * @param quietUnit       l'unità della quiet zone: <code>mm</code>, <code>in</code>, <code>mil</code>, <code>mod</code>, <code>px</code>
   * @param dpi             il valore del dpi: <code>96-600</code>
   * @param size            la dimensione dell'immagine: <code>small</code>, <code>medium</code>, <code>large</code>
   */
  public QRCodeGen(String errorCorrection, String backColor, String quietZone, String quietUnit, String dpi, String size) {
    url = new StringBuilder("https://qrcode.tec-it.com/API/QRCode?");
    url.append("errorcorrection=").append(errorCorrection);
    url.append("&backcolor=").append(Tools.urlEncode(backColor));
    url.append("&quietzone=").append(quietZone);
    url.append("&quietunit=").append(quietUnit);
    url.append("&dpi=").append(dpi);
    url.append("&size=").append(size);
  }

  /**
   * Setta il testo da codificare
   *
   * @param data il testo da codificare in qr code
   */
  public void setData(String data) {
    this.data = "&data=" + Tools.urlEncode(data);
  }

  public String getCode() {
    return url.toString() + data;
  }
}
