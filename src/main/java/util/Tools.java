package util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class Tools {
  /**
   * @param s la stringa da convertire in UTF-8
   * @return la stringa convertita in UTF-8
   */
  public static String convertUTF8(String s) {
    byte[] b = s.getBytes();
    return new String(b, StandardCharsets.UTF_8);
  }

  /**
   * @param s la stringa da codificare in url
   * @return la stringa codificata in url
   */
  public static String urlEncode(String s) {
    String text = null;
    try {
      text = URLEncoder.encode(s, StandardCharsets.UTF_8.toString());
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }
    return text;
  }
}
