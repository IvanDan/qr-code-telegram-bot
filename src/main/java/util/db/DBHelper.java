package util.db;

import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

public class DBHelper {
  private static Session session;

  public DBHelper() {
    session = new Configuration().configure()
        .addAnnotatedClass(User.class)
        .addAnnotatedClass(WiFiQR.class)
        .buildSessionFactory().openSession();
  }

  private void commit() {
    session.getTransaction().commit();
  }

  // User
  public void save(User user) {
    session.beginTransaction();
    session.saveOrUpdate(user);
    commit();
  }

  public boolean checkUser(long userId) {
    session.beginTransaction();
    User user = session.get(User.class, userId);
    commit();
    return user != null;
  }

  public void setState(long userId, String state) {
    session.beginTransaction();
    User user = session.get(User.class, userId);
    user.setState(state);
    session.update(user);
    commit();
  }

  public String getState(long userId) {
    session.beginTransaction();
    User user = session.get(User.class, userId);
    commit();
    return user.getState();
  }

  public User getUser(long userId) {
    session.beginTransaction();
    User user = session.get(User.class, userId);
    commit();
    return user;
  }
  // fine User

  // WiFi
  public void save(WiFiQR wiFiQR) {
    session.beginTransaction();
    session.saveOrUpdate(wiFiQR);
    commit();
  }

  public WiFiQR getWiFi(long userId) {
    session.beginTransaction();
    WiFiQR wiFiQR = session.get(WiFiQR.class, userId);
    commit();
    return wiFiQR;
  }

  public void setType(long userId, String type) {
    session.beginTransaction();
    WiFiQR wiFiQR = session.get(WiFiQR.class, userId);
    wiFiQR.setType(type);
    session.update(wiFiQR);
    commit();
  }

  public void setSSID(long userId, String ssid) {
    session.beginTransaction();
    WiFiQR wiFiQR = session.get(WiFiQR.class, userId);
    wiFiQR.setSSID(ssid);
    session.update(wiFiQR);
    commit();
  }

  public String getType(long userId) {
    session.beginTransaction();
    WiFiQR wiFiQR = session.get(WiFiQR.class, userId);
    commit();
    return wiFiQR.getType();
  }

  public void delete(long userId) {
    session.beginTransaction();
    WiFiQR wiFiQR = session.get(WiFiQR.class, userId);
    session.delete(wiFiQR);
    commit();
  }
  // fine WiFi
}
