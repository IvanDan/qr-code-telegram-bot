package util.db;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "wifi")
public class WiFiQR {
  @Id
  private long id;
  private String type;
  private String ssid;
  @Transient
  private String password;

  public WiFiQR(long id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getSsid() {
    return ssid;
  }

  public void setSSID(String ssid) {
    this.ssid = ssid;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    if (type != null)
      return "WIFI:T:" + type + ";S:" + ssid + ";P:" + password + ";;";
    return "WIFI:" + "S:" + ssid + ";;";
  }
}
